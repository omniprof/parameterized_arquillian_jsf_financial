package com.kfstandard.business.test.single;

import com.kfstandard.business.Calculations;
import com.kfstandard.data.FinanceBean;
import java.io.File;
import java.math.BigDecimal;
import javax.inject.Inject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Single Arquillian Test
 *
 * @author Ken Fogel
 */
@RunWith(Arquillian.class)
public class CalculationsSingleTest {

    private final static Logger LOG = LoggerFactory.getLogger(CalculationsSingleTest.class);

    @Deployment
    public static WebArchive deploy() {

        // Use an alternative to the JUnit assert library called AssertJ
        // Need to reference MySQL driver as it is not part of either
        // embedded or remote
        final File[] dependencies = Maven
                .resolver()
                .loadPomFromFile("pom.xml")
                .resolve("org.assertj:assertj-core",
                        "org.slf4j:slf4j-api",
                        "org.apache.logging.log4j:log4j-slf4j-impl",
                        "org.apache.logging.log4j:log4j-web"
                ).withTransitivity()
                .asFile();

        // The SQL script to create the database is in src/test/resources
        final WebArchive webArchive = ShrinkWrap.create(WebArchive.class, "test.war")
                .setWebXML(new File("src/main/webapp/WEB-INF/web.xml"))
                .addPackage(Calculations.class.getPackage())
                .addPackage(FinanceBean.class.getPackage())
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsResource(new File("src/main/resources/log4j2.xml"), "log4j2.xml")
                .addAsLibraries(dependencies);

        return webArchive;
    }

    @Inject
    private Calculations calculations;

    @Inject
    private FinanceBean money;

    public CalculationsSingleTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of performCalculation method, of class Calculations.
     */
    @Test
    public void testPerformCalculationLoan() {
        money.setCalculationType("Standalone Loan");
        money.setInputValue(new BigDecimal("5000"));
        money.setRate(new BigDecimal("0.05"));
        money.setTerm(new BigDecimal("60"));

        Calculations instance = new Calculations();
        calculations.performCalculation();
        LOG.debug("Money: " + money.toString());
        assertEquals(money.getResult(), new BigDecimal("94.36"));
    }

}
