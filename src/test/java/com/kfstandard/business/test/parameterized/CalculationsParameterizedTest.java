package com.kfstandard.business.test.parameterized;

import com.kfstandard.business.Calculations;
import com.kfstandard.data.FinanceBean;
import java.io.File;
import java.math.BigDecimal;
import javax.inject.Inject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Rule;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is an example or parameterized testing in Arquillian. There can only be 
 * one @RunWith annotation and we must use @RunWith(Arquillian.class) therefore 
 * we cannot use @RunWith(Parameterized.class). We overcome this by using a 
 * custom MethodRule. The one we are using was written by Aslak Knutsen who is 
 * an Arquillian developer at Red Hat.
 * 
 * @author Ken Fogel
 */
@RunWith(Arquillian.class)
public class CalculationsParameterizedTest {

    private final static Logger LOG = LoggerFactory.getLogger(CalculationsParameterizedTest.class);

    @Deployment
    public static WebArchive deploy() {

        // Use an alternative to the JUnit assert library called AssertJ
        // Need to reference MySQL driver as it is not part of either
        // embedded or remote
        final File[] dependencies = Maven
                .resolver()
                .loadPomFromFile("pom.xml")
                .resolve("org.assertj:assertj-core",
                        "org.slf4j:slf4j-api",
                        "org.apache.logging.log4j:log4j-slf4j-impl",
                        "org.apache.logging.log4j:log4j-web"
                ).withTransitivity()
                .asFile();

        // This is a simpler ShrinkWrap because this example does not use JPA
        final WebArchive webArchive = ShrinkWrap.create(WebArchive.class, "test.war")
                .setWebXML(new File("src/main/webapp/WEB-INF/web.xml"))
                // Add all classes that share the package this class is found in
                .addPackage(Calculations.class.getPackage())
                .addPackage(FinanceBean.class.getPackage())
                // Add only this class from the package it is found in
                .addClass(ParameterRule.class)
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsResource(new File("src/main/resources/log4j2.xml"), "log4j2.xml")
                .addAsLibraries(dependencies);

        return webArchive;
    }

    /**
     * The ParameterRule class does not support an array of values that are 
     * assigned by the test class constructor as regular parameterized are done.
     * 
     * ParameterRule creates FinanceBean objects initialized through the 
     * FinanceBeani constructor one at a time and then runs the test methods.
     * 
     * In this example I was able to use the FinanceBean but for your tests you 
     * will need to create a special test class that contains the parameters to 
     * test and the result
     */
    @Rule
    public ParameterRule rule = new ParameterRule("dynamic", 
            new FinanceBean(new BigDecimal("94.36"), new BigDecimal("5000"),new BigDecimal("0.05"),new BigDecimal("60"), "loan" ),
            new FinanceBean(new BigDecimal("92.08"), new BigDecimal("5000"),new BigDecimal("0.04"),new BigDecimal("60"), "loan" ),
            new FinanceBean(new BigDecimal("89.84"), new BigDecimal("5000"),new BigDecimal("0.03"),new BigDecimal("60"), "loan" ),
            new FinanceBean(new BigDecimal("87.64"), new BigDecimal("5000"),new BigDecimal("0.02"),new BigDecimal("60"), "loan" ),
            new FinanceBean(new BigDecimal("6800.61"), new BigDecimal("100"),new BigDecimal("0.05"),new BigDecimal("60"), "future" ),
            new FinanceBean(new BigDecimal("13601.22"), new BigDecimal("200"),new BigDecimal("0.05"),new BigDecimal("60"), "future" ),
            new FinanceBean(new BigDecimal("20401.82"), new BigDecimal("300"),new BigDecimal("0.05"),new BigDecimal("60"), "future" ),
            new FinanceBean(new BigDecimal("27202.43"), new BigDecimal("400"),new BigDecimal("0.05"),new BigDecimal("60"), "future" ),
            new FinanceBean(new BigDecimal("100.00"), new BigDecimal("6800.61"),new BigDecimal("0.05"),new BigDecimal("60"), "savings" ),
            new FinanceBean(new BigDecimal("200.00"), new BigDecimal("13601.22"),new BigDecimal("0.05"),new BigDecimal("60"), "savings" ),
            new FinanceBean(new BigDecimal("300.00"), new BigDecimal("20401.82"),new BigDecimal("0.05"),new BigDecimal("60"), "savings" ),
            new FinanceBean(new BigDecimal("400.00"), new BigDecimal("27202.43"),new BigDecimal("0.05"),new BigDecimal("60"), "savings" )
    );

    // The identifier for this object must match the string that is the first 
    // parameter of the ParameterRule object
    private FinanceBean dynamic;

    // These are the objects we are testing with.
    @Inject
    private Calculations calculations;

    @Inject
    private FinanceBean money;

    /**
     * The following methods are unused in this example
     */
    public CalculationsParameterizedTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Here is a basic test method.
     * We must copy the values from the dynamic object to the injected 
     * FinanceBean money object because we want to run the tests using the 
     * injected beans.
     */
    @Test
    public void testPerformCalculationLoan() {
        // Copying the local FinanceBean fields to the injected FinanceBean 
        // named money. We do not copy the result as it will be overwritten by 
        // the calculation.
        money.setCalculationType(dynamic.getCalculationType());
        money.setInputValue(dynamic.getInputValue());
        money.setRate(dynamic.getRate());
        money.setTerm(dynamic.getTerm());
       
        // This is the inject Calculations object. You would likely be using an 
        // injected JPA controller of other named bean you are testing.
        calculations.performCalculation();
        LOG.debug("Money: " + money.toString());
        
        assertEquals(money.getResult(), dynamic.getResult());
    }
}
