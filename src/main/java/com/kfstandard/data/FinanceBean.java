package com.kfstandard.data;

import java.math.BigDecimal;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@Named("money")
@RequestScoped
public class FinanceBean {

    private BigDecimal inputValue;
    private BigDecimal rate;
    private BigDecimal term;
    private BigDecimal result;
    private String calculationType;

    public FinanceBean() {
        result = new BigDecimal("0");
        inputValue = new BigDecimal("0");
        rate = new BigDecimal("0");
        term = new BigDecimal("0");
        calculationType = "loan";
    }

    /**
     * This constructor was added to support parameterized testing
     * @param result
     * @param inputValue
     * @param rate
     * @param term
     * @param calculationType 
     */
    public FinanceBean(BigDecimal result, BigDecimal inputValue, BigDecimal rate, BigDecimal term, String calculationType) {
        this.result = new BigDecimal(result.toString());
        this.inputValue = new BigDecimal(inputValue.toString());
        this.rate = new BigDecimal(rate.toString());
        this.term = new BigDecimal(term.toString());
        this.calculationType = calculationType;
    }
    
    
    public void cancel() {
        result = new BigDecimal("0");
        inputValue = new BigDecimal("0");
        rate = new BigDecimal("0");
        term = new BigDecimal("0");
        calculationType = "loan";

    }

    public BigDecimal getResult() {
        return result;
    }

    public void setResult(BigDecimal result) {
        this.result = result;
    }

    public BigDecimal getInputValue() {
        return inputValue;
    }

    public void setInputValue(BigDecimal inputValue) {
        this.inputValue = inputValue;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public BigDecimal getTerm() {
        return term;
    }

    public void setTerm(BigDecimal term) {
        this.term = term;
    }

    public String getCalculationType() {
        return calculationType;
    }

    public void setCalculationType(String calculationType) {
        this.calculationType = calculationType;
    }

    @Override
    public String toString() {
        return "FinanceBean{" + "inputValue=" + inputValue + ", rate=" + rate + ", term=" + term + ", result=" + result + ", calculationType=" + calculationType + '}';
    }

}
