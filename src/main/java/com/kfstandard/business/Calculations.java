package com.kfstandard.business;

import com.kfstandard.data.FinanceBean;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Named("calculate")
@RequestScoped
public class Calculations {

    private final static Logger LOG = LoggerFactory.getLogger(Calculations.class);

    @Inject
    FinanceBean money;

    public Calculations() {
        super();
    }

    /**
     * Based on the calculationType in the bean, select the appropriate
     * calculation and return the result as a String
     *
     * @return
     */
    public String performCalculation() {
        switch (money.getCalculationType()) {
            case "loan":
                loanCalculation();
                break;
            case "future":
                futureValueCalculation();
                break;
            case "savings":
                savingsGoalCalculation();
                break;
        }
        return null;
    }

    /**
     * Payments on a loan
     *
     * @throws ArithmeticException
     */
    private void loanCalculation() throws ArithmeticException {
        //LOG.debug("Loan");
        // Convert APR to monthly
        BigDecimal adjustedRate = money.getRate().divide(new BigDecimal("12"), MathContext.DECIMAL64);

        BigDecimal temp;
        // (1+rate)
        temp = BigDecimal.ONE.add(adjustedRate);

        // (1+rate)^term
        temp = temp.pow(money.getTerm().intValueExact());

        // BigDecimal pow does not support negative exponents so divide 1 by the result
        temp = BigDecimal.ONE.divide(temp, MathContext.DECIMAL64);

        // 1 - (1+rate)^-term
        temp = BigDecimal.ONE.subtract(temp);

        // rate / (1 - (1+rate)^-term)
        temp = adjustedRate.divide(temp, MathContext.DECIMAL64);

        // pv * (rate / 1 - (1+rate)^-term)
        temp = money.getInputValue().multiply(temp);
        
        // Round to two decimals
        temp = temp.setScale(2, RoundingMode.HALF_EVEN);
        money.setResult(temp.abs());
    }

    /**
     * Future value of money invested at regular intervals
     *
     * @throws ArithmeticException
     */
    private void futureValueCalculation() throws ArithmeticException {
        //LOG.debug("Future");
        // Convert APR to monthly
        BigDecimal adjustedRate = money.getRate().divide(new BigDecimal("12"), MathContext.DECIMAL64);

        BigDecimal temp;
        // (1+rate)
        temp = BigDecimal.ONE.add(adjustedRate);
        // (1+rate)^term
        temp = temp.pow(money.getTerm().intValueExact());
        // 1 - (1+rate)^term
        temp = BigDecimal.ONE.subtract(temp);
        // (1 - (1+rate)^-term) / rate
        temp = temp.divide(adjustedRate, MathContext.DECIMAL64);
        // pmt * ((1 - (1+rate)^-term) / rate)
        temp = money.getInputValue().multiply(temp);
        temp = temp.setScale(2, RoundingMode.HALF_EVEN);
        money.setResult(temp.abs());

    }

    /**
     * How much to set aside at regular intervals to reach a specific goal
     *
     * @throws ArithmeticException
     */
    private void savingsGoalCalculation() throws ArithmeticException {
        //LOG.debug("Savings");

        // Convert APR to monthly
        BigDecimal adjustedRate = money.getRate().divide(new BigDecimal("12"), MathContext.DECIMAL64);
        BigDecimal temp;
        // (1+rate)
        temp = BigDecimal.ONE.add(adjustedRate);
        // (1+rate)^term
        temp = temp.pow(money.getTerm().intValueExact());
        // 1 - ((1+rate)^term)
        temp = BigDecimal.ONE.subtract(temp);
        // rate / (1 - (1+rate)^term)
        temp = adjustedRate.divide(temp, MathContext.DECIMAL64);
        // fv * (rate / (1 - (1+rate)^term))
        temp = money.getInputValue().multiply(temp);
        temp = temp.setScale(2, RoundingMode.HALF_EVEN);
        money.setResult(temp.abs());
    }
}
